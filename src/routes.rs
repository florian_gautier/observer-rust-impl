use std::collections::HashMap;

use rocket::{serde::json::Json, State, http::Status};
use serde::Serialize;

use crate::events::{PendingEventQueue};

#[derive(Serialize)]
pub struct Response { 
    kind: String,
    api_version: String, 
    metadata: HashMap<String, String>,
    status: String,
    message: String, 
    code: u8,
 }

#[post("/inbox", data = "<data>")]
pub fn inbox(data: String, queue: &State<PendingEventQueue>) -> Status {
    println!("{}",data);
    let _write = queue.events.write();

    let mut evts = match _write {
        Ok(q) => q,
        Err(e) => {
            println!("Poisoined lock. Data :{} will be lost. {:?}", data, e);
            return Status::InternalServerError
        },
    };
    
    evts.push_back(data);
    Status::Ok
}

#[get("/workflows")]
pub fn workflows(queue: &State<PendingEventQueue>) -> Result<Json<Response>, Status> {
    let workflows_response = Response {
        kind: String::from("Status"),
        api_version: String::from("v1"),
        metadata: HashMap::new(),
        status: String::from("Success"),
        message: String::from("1 workflows in progress"),
        code: 200,
    };
    
    let _read = queue.events.read();

    let evts = match _read {
        Ok(q) => q,
        Err(e) => {
            println!("Poisoined lock. Data could not be fetched :{}", e);
            return Err(Status::InternalServerError)
        }
    };
    println!("{:?}", evts);
    Ok(Json(workflows_response))
}

#[get("/workflows/status")]
pub fn workflows_status()  -> Json<Response> {
    let workflows_response = Response {
        kind: String::from("Status"),
        api_version: String::from("v1"),
        metadata: HashMap::new(),
        status: String::from("Success"),
        message: String::from("No workflow in progress"),
        code: 200,
    };
    Json(workflows_response)
}

#[get("/workflows/<workflow_uuid>/status")]
pub fn workflow_status(workflow_uuid: &str) -> Json<Response> {
    println!("Requesting status for workflow {}", workflow_uuid);
    let workflow_response = Response {
        kind: String::from("Status"),
        api_version: String::from("v1"),
        metadata: HashMap::new(),
        status: String::from("Success"),
        message: String::from("Workflow completed"),
        code: 200,
    };
    Json(workflow_response)
}

#[get("/workflows/<workflow_uuid>/workers")]
pub fn workers(workflow_uuid: &str) -> Json<Response> {
    println!("Requesting worker statuses for workflow {}", workflow_uuid);
    let workflow_response = Response {
        kind: String::from("Status"),
        api_version: String::from("v1"),
        metadata: HashMap::new(),
        status: String::from("Success"),
        message: String::from("2 active workers on workflow"),
        code: 200,
    };
    Json(workflow_response)
}
