#[macro_use] extern crate rocket;


use configuration::ObserverServiceConfiguration;
use rocket::routes;

use structopt::StructOpt;

mod configuration;
mod routes;
mod subscription;
mod events;


use subscription::{SubscriptionManager};
use routes::{inbox, workflows, workflows_status, workflow_status, workers};
use events::PendingEventQueue;

#[derive(StructOpt)]
struct Opts {
    /// alternative context
    #[structopt(long)]
    context: Option<String>,
    /// alternative config file
    #[structopt(long)]
    config: Option<String>
}

fn load_configuration() -> Result<ObserverServiceConfiguration, String>{
    let opts = Opts::from_args();

    let config = match opts.config {
        Some(file) => file,
        None => String::from("config/observer.yaml"),
    };

    let configuration =configuration::ObserverServiceConfiguration::from_file(&config);

    configuration
}

async fn register_service() -> Result<SubscriptionManager, String> {
    let mut mngr = SubscriptionManager::new();
    
    let registration = match mngr.init_subscriptions().await {
        Ok(_) => {
            mngr.display_active_subscruptions();
            Ok(mngr)
        },
        Err(e) => {
            mngr.release_subscriptions().await?;
            Err(e)
        }
    };

    registration
}

async fn launch_server(configuration: &ObserverServiceConfiguration) -> Result<(),String> {
    println!("Loaded configuration {:?}", configuration);

    let opts = Opts::from_args();
    let port = match configuration.port(&opts.context) {
        Ok(p) => p,
        Err(e) => return Err(format!("No valid port configuration. {}", e))
    };

    println!("Will listen on{:?}", port);

    let figment = rocket::Config::figment() .merge(("port", port));
    let routes = routes![inbox, workflows, workflows_status, workflow_status, workers];

    let recorded_evts = PendingEventQueue::new();
    let rocket = rocket::custom(figment)
                                                    .mount("/", routes)
                                                    .manage(recorded_evts);

    let ignited_rocket= match rocket.ignite().await {
        Ok(r) =>r,
        Err(e) => return Err(format!("Something wen't wrong with rocket ignition {}",e))
    };
    
    let flight = match  ignited_rocket.launch().await {
        Ok(_) => Ok(()),
        Err(e) =>  Err(format!("Something wen't wrong with rocket flight {}",e))
    };

    flight
}


async fn cleanup(mut mngr: SubscriptionManager) -> Result<(),String> {
    mngr.release_subscriptions().await
}

#[rocket::main]
async fn main() -> Result<(),String> {

    let conf = match load_configuration() {
        Ok(c) => c,
        Err(e) => return Err(e)
    }; 

    let mngr = match register_service().await {
        Ok(m) => m,
        Err(e) => return Err(e)
    };

    let mut errors = Vec::new();
    if let Err(s) =  launch_server(&conf).await {
        errors.push(s);
    }
    if let Err(c) = cleanup(mngr).await {
        errors.push(c);
    }

    if ! errors.is_empty() {
        return Err(format!("{:?}", errors))
    }

    Ok(())
}