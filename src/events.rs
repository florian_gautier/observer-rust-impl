use std::{ sync::{Arc, RwLock}, collections::VecDeque};

#[derive(Debug)]
pub struct PendingEventQueue {
    pub events : Arc<RwLock<VecDeque<String>>>,
}


impl PendingEventQueue {
    pub fn new() -> Self { 
        PendingEventQueue {events: Arc::new(RwLock::new(VecDeque::new())) } 
    }
}
